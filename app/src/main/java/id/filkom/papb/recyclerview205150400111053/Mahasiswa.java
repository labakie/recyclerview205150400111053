package id.filkom.papb.recyclerview205150400111053;

import java.util.ArrayList;

public class Mahasiswa extends ArrayList<String> {
    public String nim;
    public String nama;

    public Mahasiswa() {

    }

    public Mahasiswa(String nim, String nama) {
        this.nim = nim;
        this.nama = nama;
    }

}
