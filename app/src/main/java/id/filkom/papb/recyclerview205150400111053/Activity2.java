package id.filkom.papb.recyclerview205150400111053;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import id.filkom.papb.recyclerview205150400111053.R;

public class Activity2 extends AppCompatActivity {

    String nim, nama;
    TextView tvNim, tvNama;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);

        tvNim = findViewById(R.id.tvNim2);
        tvNama = findViewById(R.id.tvNama2);

        getData();
    }

    public void getData() {
        nim = getIntent().getStringExtra("nim");
        nama = getIntent().getStringExtra("nama");
        tvNim.setText("NIM: " + nim);
        tvNama.setText("Nama: " + nama);
    }
}