package id.filkom.papb.recyclerview205150400111053;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import id.filkom.papb.recyclerview205150400111053.R;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    MahasiswaAdapter adapter;
    EditText etNim, etNama;
    Button btSimpan;
    RecyclerView rv1;
    ArrayList<Mahasiswa> data;
    public static String TAG = "RV1";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // find view, listener
        etNim = (EditText) findViewById(R.id.etNim);
        etNama = (EditText) findViewById(R.id.etNama);
        btSimpan = (Button) findViewById(R.id.bt1);
        btSimpan.setOnClickListener(this);
        rv1 = findViewById(R.id.rv1);

        ArrayList<Mahasiswa> data = getData();
        adapter = new MahasiswaAdapter(this, data);
        rv1.setAdapter(adapter);
        rv1.setLayoutManager(new LinearLayoutManager(this));
    }

    public ArrayList getData() {
        data = new ArrayList<>();
        List<String> nim = Arrays.asList(getResources().getStringArray(R.array.nim));
        List<String> nama = Arrays.asList(getResources().getStringArray(R.array.nama));
        for (int i = 0; i < nim.size(); i++) {
            Mahasiswa mhs = new Mahasiswa();
            mhs.nim = nim.get(i);
            mhs.nama = nama.get(i);
            Log.d(TAG,"getData "+mhs.nim);
            data.add(mhs);
        }
        return data;
    }

    // method tambah data baru
    public void addData() {
        String nimMhs = etNim.getText().toString();
        String namaMhs = etNama.getText().toString();
        // handle text kosong
        if (nimMhs.equals("") && namaMhs.equals("")) {
            Toast.makeText(this, "Semua data wajib diisi", Toast.LENGTH_SHORT).show();
        } else if (nimMhs.equals("") || namaMhs.equals("")) {
            Toast.makeText(this, "Semua data wajib diisi", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Data berhasil disimpan", Toast.LENGTH_SHORT).show();
            data.add(new Mahasiswa(nimMhs, namaMhs));
            // hapus text setelah data tersimpan
            etNim.setText("");
            etNama.setText("");
        }
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == btSimpan.getId()) {
            addData();
            adapter.notifyDataSetChanged();
        }
    }

}