# Tugas 5 - RecyclerView

## Description

<p>The main purpose of this task is to implement the recylcler view, a scrollable container for data sets that can push efficiency with its reuses of views. An explicit intent was also added to move from the recycler view to another activity.</p>

## Screenshot

<table>
    <tr>
        <td style="text-align: center">Tugas 5-RecyclerView</td>
    </tr>
    <tr>
        <td>![6wr4vz](/uploads/16ea02caf31187c55fbd9ca4dcb71516/6wr4vz.gif)</td>
    </tr>
</table>
